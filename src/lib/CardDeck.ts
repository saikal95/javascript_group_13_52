
type myObj = {
   rank : string,
   suit : string,
}

let CardObj: myObj;

export class Card {
  constructor(
    public rank : any,
    public suit : any,
  ){}

  getScore(){
   this.rank = parseInt(this.rank)
   if(this.rank === 'Q' || this.rank === 'J' || this.rank  === 'K'){
     return 10;
   } else if(this.rank === 'A'){
     return 11;
   }
   return this.rank;
  }
}


export class CardDeck {
  cards:  {}[] = [];
  secondCard: {}[] = [];
  RANKS: string[]  = ['2', '3', '4', '5', '6', '7', '8', '9','10', 'J', 'Q', 'K', 'A'];
  SUITS: string[] = ['diams', 'hearts', 'clubs', 'spades'];

  constructor() {
    for (let i = 0; i < this.RANKS.length; i++) {
      for (let j = 0; j < this.SUITS.length; j++) {
        CardObj = {
          rank: this.RANKS[i],
          suit: this.SUITS[j]
        }
        this.cards.push(CardObj);
      }
    }
  }

  getCard(){
    const cardIndx = Math.floor(Math.random()* this.cards.length);
    this.cards.splice(cardIndx,1)
    return this.cards[cardIndx];
  }

  getCards(howMany: number){
    const newArray = [];
   for(let i = 0; i <  howMany; i++){
      newArray.push(this.getCard())
   }
   this.secondCard = newArray;
    return this.secondCard;
  }

}
