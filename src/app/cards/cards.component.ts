import {Component, Input} from '@angular/core';
import {CardDeck, Card} from "../../lib/CardDeck";

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent {
  @Input()  rank='';
  @Input() suit='';
  public card: any;
  constructor() {

  }

  getSecondClass(){
      this.card = `card ${this.rank.toLowerCase()} ${this.suit}`;
      return this.card;
  }


 getSuitName(suit: string){
   if(this.suit ==='diams'){
     return '♦'
   }else if(this.suit === 'hearts'){
      return '♥';
   }else if(this.suit === 'clubs'){
     return '♣'
   } else if(this.suit === 'spades'){
     return ' ♠'
   }
   return 'sdf'
 }



}
